from django.db import models

class Team(models.Model):
    id_team = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    venue = models.CharField(max_length=100, default="unknown")
    owner = models.CharField(max_length=100, default="unknown")

class Player(models.Model):
    id_player = models.AutoField(primary_key=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    position = models.CharField(max_length=50)

class Stadium(models.Model):
    id_stadium = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    capacity = models.IntegerField()
    owner = models.CharField(max_length=100)
