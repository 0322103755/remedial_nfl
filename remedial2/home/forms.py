from django import forms
from .models import Team, Player, Stadium

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['name', 'venue', 'owner']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Team Name'}),
            'venue': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Venue'}),
            'owner': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Owner'}),
}

class TeamUpdateForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['name', 'venue', 'owner']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Team Name'}),
            'venue': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Venue'}),
            'owner': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Owner'}),
}
        
class TeamDeleteForm(forms.Form):
    confirm_delete = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        help_text='Check this box to confirm deletion'
    )

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ['name', 'number', 'position', 'team']

    def __init__(self, *args, **kwargs):
        super(PlayerForm, self).__init__(*args, **kwargs)


class PlayerDeleteForm(forms.Form):
    confirm_delete = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        help_text='Check this box to confirm deletion'
    )


class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = ['name', 'capacity', 'owner']

    def __init__(self, *args, **kwargs):
        super(StadiumForm, self).__init__(*args, **kwargs)

class StadiumDeleteForm(forms.Form):
    confirm_delete = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}),
        help_text='Check this box to confirm deletion'
    )