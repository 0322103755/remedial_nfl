from django.urls import path
from .views import (
    TeamListView, TeamDetailView, TeamCreateView, TeamUpdateView, TeamDeleteView,
    all_teams, all_owners, all_players, players_of_team, players_info_of_team,
    PlayerListView, PlayerDetailView, PlayerCreateView, PlayerUpdateView, PlayerDeleteView,
    StadiumListView, StadiumDetailView, StadiumCreateView, StadiumUpdateView, StadiumDeleteView,
    all_teams, all_owners, all_players, players_of_team, players_info_of_team,
    all_stadiums, owner_and_stadium
)

app_name = 'home'

urlpatterns = [
    # Team 
    path('teams/', TeamListView.as_view(), name='team_list'),
    path('teams/new/', TeamCreateView.as_view(), name='team_create'),
    path('teams/<int:pk>/edit/', TeamUpdateView.as_view(), name='team_update'),
    path('teams/<int:pk>/delete/', TeamDeleteView.as_view(), name='team_delete'),
    path('all-teams/', all_teams, name='all_teams'),
    path('all-owners/', all_owners, name='all_owners'),
    path('all-players/', all_players, name='all_players'),
    path('teams/<int:team_id>/players/', players_of_team, name='players_of_team'),
    path('teams/<int:team_id>/players-info/', players_info_of_team, name='players_info_of_team'),

    # Player 
    path('players/', PlayerListView.as_view(), name='player_list'),
    path('players/<int:pk>/', PlayerDetailView.as_view(), name='player_detail'),
    path('players/new/', PlayerCreateView.as_view(), name='player_create'),
    path('players/<int:pk>/edit/', PlayerUpdateView.as_view(), name='player_update'),
    path('players/<int:pk>/delete/', PlayerDeleteView.as_view(), name='player_delete'),

    # Stadium 
    path('stadiums/', StadiumListView.as_view(), name='stadium_list'),
    path('stadiums/<int:pk>/', StadiumDetailView.as_view(), name='stadium_detail'),
    path('stadiums/new/', StadiumCreateView.as_view(), name='stadium_create'),
    path('stadiums/<int:pk>/edit/', StadiumUpdateView.as_view(), name='stadium_update'),
    path('stadiums/<int:pk>/delete/', StadiumDeleteView.as_view(), name='stadium_delete'),

    #Extra Queries
    path('all-teams/', all_teams, name='all_teams'),
    path('all-owners/', all_owners, name='all_owners'),
    path('all-players/', all_players, name='all_players'),
    path('all-stadiums/', all_stadiums, name='all_stadiums'),
    path('team/<int:team_id>/players/', players_of_team, name='players_of_team'),
    path('owner-and-stadium/<str:owner_name>/', owner_and_stadium, name='owner_and_stadium'),
]