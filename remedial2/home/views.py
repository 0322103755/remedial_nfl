from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Team, Player, Stadium
from .forms import TeamForm, TeamUpdateForm, TeamDeleteForm, StadiumDeleteForm, PlayerDeleteForm

class TeamListView(ListView):
   model = Team
   template_name = 'team_list.html'
   context_object_name = 'teams' 
   paginate_by = 10

   
class TeamDetailView(DetailView):
    model = Team
    template_name = 'team_detail.html'
    success_url = reverse_lazy('home:team_list')

class TeamCreateView(CreateView):
    model = Team
    template_name = 'home/team_form.html'
    fields = ['name', 'venue', 'owner']

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)

class TeamUpdateView(UpdateView):
    model = Team
    template_name = 'home/team_form.html'
    fields = ['name', 'venue', 'owner']
    success_url = reverse_lazy('home:team_list')

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)

class TeamDeleteView(DeleteView):
    model = Team
    template_name = 'home/team_confirm_delete.html'
    success_url = reverse_lazy('home:team_list')
    form_class = TeamDeleteForm

    def form_valid(self, form):
        team=self.get_object()
        if form.cleaned_data['confirm_delete']:
            team.delete()
        return redirect('home:team_list')

class PlayerListView(ListView):
    model = Player
    template_name = 'home/player_list.html'
    context_object_name = 'players' 
    paginate_by = 10

class PlayerDetailView(DetailView):
    model = Player
    template_name = 'home/player_detail.html'

class PlayerCreateView(CreateView):
    model = Player
    template_name = 'home/player_form.html'
    fields = ['team', 'name', 'number', 'position']

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)


class PlayerUpdateView(UpdateView):
    model = Player
    template_name = 'home/player_form.html'
    fields = ['team', 'name', 'number', 'position']

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)


class PlayerDeleteView(DeleteView):
    model = Player
    template_name = 'home/player_confirm_delete.html'
    success_url = reverse_lazy('home:player_list')
    form_class = PlayerDeleteForm

    def form_valid(self, form):
        player = self.get_object()
        if form.cleaned_data['confirm_delete']:
            player.delete()
            return redirect('home:player_list')

#STADIUM
class StadiumListView(ListView):
    model = Stadium
    template_name = 'home/stadium_list.html'
    context_object_name = 'teams' 
    paginate_by = 10

class StadiumDetailView(DetailView):
    model = Stadium
    template_name = 'home/stadium_detail.html'
    
class StadiumCreateView(CreateView):
    model = Stadium
    template_name = 'home/stadium_form.html'
    fields = ['name', 'capacity', 'owner']
    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)
    
class StadiumUpdateView(UpdateView):
    model = Stadium
    template_name = 'home/stadium_form.html'
    fields = ['name', 'capacity', 'owner']

    def form_valid(self, form):
        self.object = form.save()
        context = self.get_context_data()
        context["success"] = True
        return render(self.request, self.template_name, context)

class StadiumDeleteView(DeleteView):
    model = Stadium
    template_name = 'home/stadium_confirm_delete.html'
    success_url = reverse_lazy('home/stadium_list')
    form_class = StadiumDeleteForm

    def form_valid(self, form):
        stadium = self.get_object()
        if form.cleaned_data['confirm_delete']:
            stadium.delete()
        return redirect('home:stadium_list')
# EXTRA QUERIES

def all_teams(request):
    teams = Team.objects.all()
    return render(request, 'home/all_teams.html', {'teams': teams})

def all_owners(request):
    owners = Team.objects.values_list('owner', flat=True).distinct()
    return render(request, 'home/all_owners.html', {'owners': owners})

def all_players(request):
    players = Player.objects.all()
    return render(request, 'home/all_players.html', {'players': players})

def players_of_team(request, team_id):
    team = get_object_or_404(Team, id_team=team_id)
    players = Player.objects.filter(team=team)
    return render(request, 'home/players_of_team.html', {'team': team, 'players': players})

def players_info_of_team(request, team_id):
    team = get_object_or_404(Team, id_team=team_id)
    players = Player.objects.filter(team=team)
    return render(request, 'home/players_info_of_team.html', {'team': team, 'players': players})

def all_stadiums(request):
    stadiums = Stadium.objects.all()
    return render(request, 'home/all_stadiums.html', {'stadiums': stadiums})

def owner_and_stadium(request, owner_name):
    team = Team.objects.filter(owner=owner_name).first()
    stadium = Stadium.objects.filter(owner=owner_name).first()
    return render(request, 'home/owner_and_stadium.html', {'owner_name': owner_name, 'stadium': stadium})